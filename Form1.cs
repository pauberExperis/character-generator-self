﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Character;
using System.Data.SqlClient;



namespace RPGCharGen
{

    public partial class Form1 : Form
    {
        Character.Character lol;
        public string currentClass = "";
        public Form1()
        {
            
            InitializeComponent();

        }

        private void btn_warrior_Click(object sender, EventArgs e)
        {
            btn_sub_1.Text = "Barbarian";
            btn_sub_2.Text = "Warden";
            btn_sub_1.Visible = true;
            btn_sub_2.Visible = true;
        }


        private void btn_thief_Click(object sender, EventArgs e)
        {
            btn_sub_1.Text = "Rogue";
            btn_sub_2.Text = "Ninja";
            btn_sub_1.Visible = true;
            btn_sub_2.Visible = true;
        }

        private void btn_wizard_Click(object sender, EventArgs e)
        {
            btn_sub_1.Text = "Spellcaster";
            btn_sub_2.Text = "Druid";
            btn_sub_1.Visible = true;
            btn_sub_2.Visible = true;
        }

        private void btn_sub_2_Click(object sender, EventArgs e)
        {
            currentClass = btn_sub_2.Text;
            label3.Text = currentClass;
            label3.Visible = true;

        }

        private void btn_sub_1_Click(object sender, EventArgs e)
        {
            currentClass = btn_sub_1.Text;
            label3.Text = currentClass;
            label3.Visible = true;
        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            if (txt_name.TextLength == 0)
            {
                MessageBox.Show("You have to write a name!");
            } else if (currentClass.Length == 0)
            {
                MessageBox.Show("You have to choose a sub-class!");
            } else
            {
                switch(currentClass.ToLower())
                {
                    case "barbarian":
                        Character.Barbarian newBarbarian = new Character.Barbarian(txt_name.Text);
                        lbl_resource.Text = "Rage:";
                        lbl_energy.Text = newBarbarian.Rage.ToString();
                        displayCharacter<Character.Barbarian>(newBarbarian);
                        break;
                    case "warden":
                        Character.Warden newWarden = new Character.Warden(txt_name.Text);
                        lbl_resource.Text = "Rage:";
                        lbl_energy.Text = newWarden.Rage.ToString();
                        displayCharacter<Character.Warden>(newWarden);
                        break;
                    case "spellcaster":
                        Character.Spellcaster newSpellcaster = new Character.Spellcaster(txt_name.Text);
                        lbl_resource.Text = "Mana:";
                        lbl_energy.Text = newSpellcaster.Mana.ToString();
                        displayCharacter<Character.Spellcaster>(newSpellcaster);
                        lbl_resource.Text = "Mana:";
                        lbl_energy.Text = newSpellcaster.Mana.ToString();
                        break;
                    case "druid":
                        Character.Druid newDruid = new Character.Druid(txt_name.Text);
                        lbl_resource.Text = "Mana:";
                        lbl_energy.Text = newDruid.Mana.ToString();
                        displayCharacter<Character.Druid>(newDruid);
                        break;
                    case "rogue":
                        lbl_resource.Text = "Energy:";
                        Character.Rogue newRogue = new Character.Rogue(txt_name.Text);
                        lbl_energy.Text = newRogue.Energy.ToString();
                        displayCharacter<Character.Rogue>(newRogue);
                        break;
                    case "ninja":
                        lbl_resource.Text = "Energy:";
                        Character.Ninja newNinja = new Character.Ninja(txt_name.Text);
                        lbl_energy.Text = newNinja.Energy.ToString();
                        displayCharacter<Character.Ninja>(newNinja);
                        break;
                    default:
                        break;
                }
            }
        }

        private void displayCharacter<T>(Character.Character type)
        {
            string classType = type.GetType().ToString();
            classType = classType.Split('.').Last();
            lbl_armour.Text = type.Armor.ToString();
            lbl_class.Text = classType;
            lbl_dexterity.Text = type.Agility.ToString();
            //lbl_energy.Text = type.Energy.ToString();
            lbl_hp.Text = type.HP.ToString();
            lbl_intelligence.Text = type.Intelligence.ToString();
            lbl_name.Text = type.Name;
            lbl_strength.Text = type.Strength.ToString();
            btn_new.Visible = true;
            grp_sum.Visible = true;
            btn_attack.Visible = true;
            btn_left.Visible = true;
            btn_forward.Visible = true;
            btn_backward.Visible = true;
            btn_right.Visible = true;
            lol = type;

            string fileName = @"../../CharacterFiles/" + lbl_name.Text + ".txt";
            FileInfo fi = new FileInfo(fileName);

            try
            {
                if (fi.Exists)
                {
                    fi.Delete();
                }

                using (StreamWriter sw = fi.CreateText())
                {
                    sw.WriteLine(lbl_name.Text + " the " + classType + "!");
                    sw.WriteLine("HP: " + lbl_hp.Text);
                    sw.WriteLine(lbl_resource.Text + " " + lbl_energy.Text);
                    sw.WriteLine("Armour: " + lbl_armour.Text);
                    sw.WriteLine("Strength: " + lbl_strength.Text);
                    sw.WriteLine("Intelligence: " + lbl_intelligence.Text);
                    sw.WriteLine("Agility: " + lbl_dexterity.Text);
                }
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                        Console.WriteLine(s);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = @"PC7364\SQLEXPRESS";
                builder.InitialCatalog = "RPGChar";
                builder.IntegratedSecurity = true;
                Console.WriteLine("Connecting to SQL Server...");

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    Console.WriteLine("done. \n");

                    string sql = "insert into heroes(name, class," + lbl_resource.Text.ToLower().Split(':').First() + 
                        ", armour, hp, strength, agility, intelligence) VALUES('" + lbl_name.Text + "','"
                        + classType + "'," + lbl_energy.Text.ToString() + "," + lbl_armour.Text + "," + lbl_hp.Text + "," + lbl_strength.Text
                        + "," + lbl_dexterity.Text + "," + lbl_intelligence.Text + ");";
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                        Console.WriteLine("Data inserted");
                    }
                }
            } catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        

        private void btn_new_Click(object sender, EventArgs e)
        {
            txt_name.Text = "";
            grp_sum.Visible = false;
            btn_new.Visible = false;
            btn_sub_1.Visible = false;
            btn_sub_2.Visible = false;
            currentClass = "";
            label3.Text = "";
            btn_attack.Visible = false;
            btn_left.Visible = false ;
            btn_forward.Visible = false;
            btn_backward.Visible = false;
            btn_right.Visible = false;
        }

        private void btn_attack_Click(object sender, EventArgs e)
        {
            lol.Attack();
        }

        private void btn_forward_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(lol.Name + " moves ");
            lol.Move(move.Forward);
        }

        private void btn_left_Click(object sender, EventArgs e)
        {
            lol.Move(move.Left);
        }

        private void btn_backward_Click(object sender, EventArgs e)
        {
            lol.Move(move.Backward);
        }

        private void btn_right_Click(object sender, EventArgs e)
        {
            lol.Move(move.Right);
        }

        private void grp_sum_Enter(object sender, EventArgs e)
        {

        }
    }
}
