﻿namespace RPGCharGen
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btn_warrior = new System.Windows.Forms.Button();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_wizard = new System.Windows.Forms.Button();
            this.btn_thief = new System.Windows.Forms.Button();
            this.btn_sub_1 = new System.Windows.Forms.Button();
            this.btn_sub_2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_submit = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lbl_dexterity = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl_intelligence = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lbl_strength = new System.Windows.Forms.Label();
            this.lbl_resource = new System.Windows.Forms.Label();
            this.lbl_armour = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_energy = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl_hp = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_class = new System.Windows.Forms.Label();
            this.btn_new = new System.Windows.Forms.Button();
            this.lbl_name = new System.Windows.Forms.Label();
            this.grp_sum = new System.Windows.Forms.GroupBox();
            this.btn_backward = new System.Windows.Forms.Button();
            this.btn_right = new System.Windows.Forms.Button();
            this.btn_left = new System.Windows.Forms.Button();
            this.btn_forward = new System.Windows.Forms.Button();
            this.btn_attack = new System.Windows.Forms.Button();
            this.grp_sum.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_warrior
            // 
            this.btn_warrior.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_warrior.Location = new System.Drawing.Point(12, 115);
            this.btn_warrior.Name = "btn_warrior";
            this.btn_warrior.Size = new System.Drawing.Size(239, 77);
            this.btn_warrior.TabIndex = 0;
            this.btn_warrior.Text = "Warrior";
            this.btn_warrior.UseVisualStyleBackColor = true;
            this.btn_warrior.Click += new System.EventHandler(this.btn_warrior_Click);
            // 
            // txt_name
            // 
            this.txt_name.BackColor = System.Drawing.SystemColors.Info;
            this.txt_name.Location = new System.Drawing.Point(60, 48);
            this.txt_name.Name = "txt_name";
            this.txt_name.Size = new System.Drawing.Size(663, 26);
            this.txt_name.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(382, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "NAME:";
            // 
            // btn_wizard
            // 
            this.btn_wizard.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_wizard.Location = new System.Drawing.Point(275, 115);
            this.btn_wizard.Name = "btn_wizard";
            this.btn_wizard.Size = new System.Drawing.Size(239, 77);
            this.btn_wizard.TabIndex = 6;
            this.btn_wizard.Text = "Wizard";
            this.btn_wizard.UseVisualStyleBackColor = true;
            this.btn_wizard.Click += new System.EventHandler(this.btn_wizard_Click);
            // 
            // btn_thief
            // 
            this.btn_thief.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_thief.Location = new System.Drawing.Point(540, 115);
            this.btn_thief.Name = "btn_thief";
            this.btn_thief.Size = new System.Drawing.Size(239, 77);
            this.btn_thief.TabIndex = 7;
            this.btn_thief.Text = "Thief";
            this.btn_thief.UseVisualStyleBackColor = true;
            this.btn_thief.Click += new System.EventHandler(this.btn_thief_Click);
            // 
            // btn_sub_1
            // 
            this.btn_sub_1.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sub_1.Location = new System.Drawing.Point(165, 236);
            this.btn_sub_1.Name = "btn_sub_1";
            this.btn_sub_1.Size = new System.Drawing.Size(239, 77);
            this.btn_sub_1.TabIndex = 8;
            this.btn_sub_1.Text = "Sub";
            this.btn_sub_1.UseVisualStyleBackColor = true;
            this.btn_sub_1.Visible = false;
            this.btn_sub_1.Click += new System.EventHandler(this.btn_sub_1_Click);
            // 
            // btn_sub_2
            // 
            this.btn_sub_2.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sub_2.Location = new System.Drawing.Point(442, 236);
            this.btn_sub_2.Name = "btn_sub_2";
            this.btn_sub_2.Size = new System.Drawing.Size(239, 77);
            this.btn_sub_2.TabIndex = 9;
            this.btn_sub_2.Text = "Sub";
            this.btn_sub_2.UseVisualStyleBackColor = true;
            this.btn_sub_2.Visible = false;
            this.btn_sub_2.Click += new System.EventHandler(this.btn_sub_2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(336, 326);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 31);
            this.label2.TabIndex = 10;
            this.label2.Text = "Chosen Class:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tempus Sans ITC", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(363, 355);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 31);
            this.label3.TabIndex = 11;
            this.label3.Text = "label3";
            this.label3.Visible = false;
            // 
            // btn_submit
            // 
            this.btn_submit.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_submit.Location = new System.Drawing.Point(601, 385);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(159, 50);
            this.btn_submit.TabIndex = 12;
            this.btn_submit.Text = "Submit";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label13.Location = new System.Drawing.Point(38, 169);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 23);
            this.label13.TabIndex = 17;
            this.label13.Text = "Dexterity";
            // 
            // lbl_dexterity
            // 
            this.lbl_dexterity.AutoSize = true;
            this.lbl_dexterity.BackColor = System.Drawing.Color.Transparent;
            this.lbl_dexterity.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dexterity.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_dexterity.Location = new System.Drawing.Point(163, 173);
            this.lbl_dexterity.Name = "lbl_dexterity";
            this.lbl_dexterity.Size = new System.Drawing.Size(81, 23);
            this.lbl_dexterity.TabIndex = 20;
            this.lbl_dexterity.Text = "Wisdom";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label10.Location = new System.Drawing.Point(38, 152);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 23);
            this.label10.TabIndex = 14;
            this.label10.Text = "Intelligence:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label9.Location = new System.Drawing.Point(38, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 23);
            this.label9.TabIndex = 13;
            this.label9.Text = "Strength:";
            // 
            // lbl_intelligence
            // 
            this.lbl_intelligence.AutoSize = true;
            this.lbl_intelligence.BackColor = System.Drawing.Color.Transparent;
            this.lbl_intelligence.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_intelligence.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_intelligence.Location = new System.Drawing.Point(163, 153);
            this.lbl_intelligence.Name = "lbl_intelligence";
            this.lbl_intelligence.Size = new System.Drawing.Size(81, 23);
            this.lbl_intelligence.TabIndex = 23;
            this.lbl_intelligence.Text = "Wisdom";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label8.Location = new System.Drawing.Point(38, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 23);
            this.label8.TabIndex = 12;
            this.label8.Text = "Armour";
            // 
            // lbl_strength
            // 
            this.lbl_strength.AutoSize = true;
            this.lbl_strength.BackColor = System.Drawing.Color.Transparent;
            this.lbl_strength.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_strength.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_strength.Location = new System.Drawing.Point(163, 133);
            this.lbl_strength.Name = "lbl_strength";
            this.lbl_strength.Size = new System.Drawing.Size(81, 23);
            this.lbl_strength.TabIndex = 24;
            this.lbl_strength.Text = "Wisdom";
            // 
            // lbl_resource
            // 
            this.lbl_resource.AutoSize = true;
            this.lbl_resource.BackColor = System.Drawing.Color.Transparent;
            this.lbl_resource.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_resource.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_resource.Location = new System.Drawing.Point(38, 92);
            this.lbl_resource.Name = "lbl_resource";
            this.lbl_resource.Size = new System.Drawing.Size(74, 23);
            this.lbl_resource.TabIndex = 11;
            this.lbl_resource.Text = "Energy:";
            // 
            // lbl_armour
            // 
            this.lbl_armour.AutoSize = true;
            this.lbl_armour.BackColor = System.Drawing.Color.Transparent;
            this.lbl_armour.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_armour.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_armour.Location = new System.Drawing.Point(163, 113);
            this.lbl_armour.Name = "lbl_armour";
            this.lbl_armour.Size = new System.Drawing.Size(81, 23);
            this.lbl_armour.TabIndex = 25;
            this.lbl_armour.Text = "Wisdom";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.Location = new System.Drawing.Point(38, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 23);
            this.label6.TabIndex = 10;
            this.label6.Text = "HP:";
            // 
            // lbl_energy
            // 
            this.lbl_energy.AutoSize = true;
            this.lbl_energy.BackColor = System.Drawing.Color.Transparent;
            this.lbl_energy.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_energy.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_energy.Location = new System.Drawing.Point(163, 93);
            this.lbl_energy.Name = "lbl_energy";
            this.lbl_energy.Size = new System.Drawing.Size(81, 23);
            this.lbl_energy.TabIndex = 26;
            this.lbl_energy.Text = "Wisdom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label5.Location = new System.Drawing.Point(38, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 23);
            this.label5.TabIndex = 9;
            this.label5.Text = "Class";
            // 
            // lbl_hp
            // 
            this.lbl_hp.AutoSize = true;
            this.lbl_hp.BackColor = System.Drawing.Color.Transparent;
            this.lbl_hp.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_hp.Location = new System.Drawing.Point(163, 73);
            this.lbl_hp.Name = "lbl_hp";
            this.lbl_hp.Size = new System.Drawing.Size(81, 23);
            this.lbl_hp.TabIndex = 27;
            this.lbl_hp.Text = "Wisdom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(38, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Name:";
            // 
            // lbl_class
            // 
            this.lbl_class.AutoSize = true;
            this.lbl_class.BackColor = System.Drawing.Color.Transparent;
            this.lbl_class.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_class.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_class.Location = new System.Drawing.Point(163, 53);
            this.lbl_class.Name = "lbl_class";
            this.lbl_class.Size = new System.Drawing.Size(81, 23);
            this.lbl_class.TabIndex = 28;
            this.lbl_class.Text = "Wisdom";
            // 
            // btn_new
            // 
            this.btn_new.BackColor = System.Drawing.SystemColors.Info;
            this.btn_new.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_new.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_new.Location = new System.Drawing.Point(591, 362);
            this.btn_new.Name = "btn_new";
            this.btn_new.Size = new System.Drawing.Size(165, 42);
            this.btn_new.TabIndex = 7;
            this.btn_new.Text = "New Character";
            this.btn_new.UseVisualStyleBackColor = false;
            this.btn_new.Visible = false;
            this.btn_new.Click += new System.EventHandler(this.btn_new_Click);
            // 
            // lbl_name
            // 
            this.lbl_name.AutoSize = true;
            this.lbl_name.BackColor = System.Drawing.Color.Transparent;
            this.lbl_name.Font = new System.Drawing.Font("Tempus Sans ITC", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_name.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbl_name.Location = new System.Drawing.Point(158, 32);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(81, 23);
            this.lbl_name.TabIndex = 29;
            this.lbl_name.Text = "Wisdom";
            // 
            // grp_sum
            // 
            this.grp_sum.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grp_sum.BackgroundImage")));
            this.grp_sum.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grp_sum.Controls.Add(this.btn_backward);
            this.grp_sum.Controls.Add(this.btn_right);
            this.grp_sum.Controls.Add(this.btn_left);
            this.grp_sum.Controls.Add(this.btn_forward);
            this.grp_sum.Controls.Add(this.btn_attack);
            this.grp_sum.Controls.Add(this.lbl_name);
            this.grp_sum.Controls.Add(this.btn_new);
            this.grp_sum.Controls.Add(this.lbl_class);
            this.grp_sum.Controls.Add(this.label4);
            this.grp_sum.Controls.Add(this.lbl_hp);
            this.grp_sum.Controls.Add(this.label5);
            this.grp_sum.Controls.Add(this.lbl_energy);
            this.grp_sum.Controls.Add(this.label6);
            this.grp_sum.Controls.Add(this.lbl_armour);
            this.grp_sum.Controls.Add(this.lbl_resource);
            this.grp_sum.Controls.Add(this.lbl_strength);
            this.grp_sum.Controls.Add(this.label8);
            this.grp_sum.Controls.Add(this.lbl_intelligence);
            this.grp_sum.Controls.Add(this.label9);
            this.grp_sum.Controls.Add(this.label10);
            this.grp_sum.Controls.Add(this.lbl_dexterity);
            this.grp_sum.Controls.Add(this.label13);
            this.grp_sum.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grp_sum.Location = new System.Drawing.Point(3, -4);
            this.grp_sum.Name = "grp_sum";
            this.grp_sum.Size = new System.Drawing.Size(800, 448);
            this.grp_sum.TabIndex = 31;
            this.grp_sum.TabStop = false;
            this.grp_sum.Text = "Character Summary";
            this.grp_sum.Visible = false;
            this.grp_sum.Enter += new System.EventHandler(this.grp_sum_Enter);
            // 
            // btn_backward
            // 
            this.btn_backward.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_backward.Location = new System.Drawing.Point(471, 224);
            this.btn_backward.Name = "btn_backward";
            this.btn_backward.Size = new System.Drawing.Size(109, 42);
            this.btn_backward.TabIndex = 34;
            this.btn_backward.Text = "Backward";
            this.btn_backward.UseVisualStyleBackColor = true;
            this.btn_backward.Visible = false;
            this.btn_backward.Click += new System.EventHandler(this.btn_backward_Click);
            // 
            // btn_right
            // 
            this.btn_right.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_right.Location = new System.Drawing.Point(580, 162);
            this.btn_right.Name = "btn_right";
            this.btn_right.Size = new System.Drawing.Size(109, 42);
            this.btn_right.TabIndex = 33;
            this.btn_right.Text = "Right";
            this.btn_right.UseVisualStyleBackColor = true;
            this.btn_right.Visible = false;
            this.btn_right.Click += new System.EventHandler(this.btn_right_Click);
            // 
            // btn_left
            // 
            this.btn_left.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_left.Location = new System.Drawing.Point(365, 162);
            this.btn_left.Name = "btn_left";
            this.btn_left.Size = new System.Drawing.Size(109, 42);
            this.btn_left.TabIndex = 32;
            this.btn_left.Text = "Left";
            this.btn_left.UseVisualStyleBackColor = true;
            this.btn_left.Visible = false;
            this.btn_left.Click += new System.EventHandler(this.btn_left_Click);
            // 
            // btn_forward
            // 
            this.btn_forward.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_forward.Location = new System.Drawing.Point(471, 101);
            this.btn_forward.Name = "btn_forward";
            this.btn_forward.Size = new System.Drawing.Size(109, 42);
            this.btn_forward.TabIndex = 31;
            this.btn_forward.Text = "Forward";
            this.btn_forward.UseVisualStyleBackColor = true;
            this.btn_forward.Visible = false;
            this.btn_forward.Click += new System.EventHandler(this.btn_forward_Click);
            // 
            // btn_attack
            // 
            this.btn_attack.BackColor = System.Drawing.Color.GhostWhite;
            this.btn_attack.Font = new System.Drawing.Font("Tempus Sans ITC", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attack.Location = new System.Drawing.Point(591, 28);
            this.btn_attack.Name = "btn_attack";
            this.btn_attack.Size = new System.Drawing.Size(165, 42);
            this.btn_attack.TabIndex = 30;
            this.btn_attack.Text = "Attack";
            this.btn_attack.UseVisualStyleBackColor = false;
            this.btn_attack.Visible = false;
            this.btn_attack.Click += new System.EventHandler(this.btn_attack_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.grp_sum);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_sub_2);
            this.Controls.Add(this.btn_sub_1);
            this.Controls.Add(this.btn_thief);
            this.Controls.Add(this.btn_wizard);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.btn_warrior);
            this.Name = "Form1";
            this.Text = "Character Generation";
            this.grp_sum.ResumeLayout(false);
            this.grp_sum.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_warrior;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_wizard;
        private System.Windows.Forms.Button btn_thief;
        private System.Windows.Forms.Button btn_sub_1;
        private System.Windows.Forms.Button btn_sub_2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbl_dexterity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_intelligence;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_strength;
        private System.Windows.Forms.Label lbl_resource;
        private System.Windows.Forms.Label lbl_armour;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbl_energy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbl_hp;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_class;
        private System.Windows.Forms.Button btn_new;
        private System.Windows.Forms.Label lbl_name;
        private System.Windows.Forms.GroupBox grp_sum;
        private System.Windows.Forms.Button btn_backward;
        private System.Windows.Forms.Button btn_right;
        private System.Windows.Forms.Button btn_left;
        private System.Windows.Forms.Button btn_forward;
        private System.Windows.Forms.Button btn_attack;
    }
}

